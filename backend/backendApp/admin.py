from django.contrib import admin

from backendApp.models import Profile, BookCopie, Book

# Register your models here.
admin.site.register(Profile)
admin.site.register(BookCopie)
admin.site.register(Book)