## Helper file to geocode address using nominatim

import urllib, simplejson
from django.utils.encoding import smart_str

def get_lat_lng(location):
    
    location = urllib.parse.quote_plus(smart_str(location))
    url = 'https://nominatim.openstreetmap.org/search?format=json&q=France+' + location
    response = urllib.request.urlopen(url).read()
    result = simplejson.loads(response)
    if result != '':
       lat =str(result[0]['lat'])
       lng =str(result[0]['lon'])
       return '%s,%s' % (lat, lng)
    else:
        return 'error'