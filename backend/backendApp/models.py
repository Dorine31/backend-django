from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.core.exceptions import ValidationError
from phone_field import PhoneField
from backendApp.helpers import get_lat_lng ## Calling helper file

# Create your models here.
def validate_hash(value):
    import re
    reg = re.compile('^([0-9]{5})')
    if not reg.match(value) :
        raise ValidationError('code invalide')

class Profile(models.Model):
    user = models.OneToOneField(User, null = True, on_delete=models.CASCADE)
    town = models.CharField(max_length=250, null = True)
    zipCode = models.CharField(validators=[validate_hash], max_length=5)
    phone = PhoneField(blank=True, help_text='Contact phone number')
    latlng = models.CharField(blank=True, max_length=100) 

    def __str__(self):
        return str(self.user)

    ## Geocode by just using zipcode 
    def _get_geo_address(self):
        return u'%s %s' % (self.zipCode, self.town)
    geo_address = property(_get_geo_address)

    def save(self, *args, **kwargs):
        if not self.latlng:
             location = self.geo_address
             self.latlng = get_lat_lng(location)
        super(Profile, self).save(*args, **kwargs)

class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255, null = True)
    category = models.CharField(max_length=255, null = True)
    description = models.TextField(null = True)
    isbn = models.CharField(max_length=13, unique=True, null = True)
    img = models.URLField(max_length=250, null = True)

    def __str__(self):
        return self.title

class BookCopie(models.Model):
    NEW = 'new'
    GOOD = 'good'
    DAMAGED = 'damaged'
    BOOK_CONDITION_CHOICES = [
        (NEW,'neuf'), 
        (GOOD,'en bon état'), 
        (DAMAGED,'abîmé')
    ]
    bookCondition = models.CharField(max_length=15, choices=BOOK_CONDITION_CHOICES, default=GOOD)
    availability = models.BooleanField(default=False)
    createdAt = models.DateTimeField(auto_now_add=True, blank=True)
    userId = models.ForeignKey(User, null = True, related_name = 'userId',on_delete = models.CASCADE)
    bookId = models.ForeignKey(Book, null = True, related_name = 'bookId',on_delete = models.CASCADE)

    def __str__(self):
        return self.bookId.title or ''
