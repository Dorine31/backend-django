from rest_framework import serializers
from django.contrib.auth.models import User
from backendApp.models import BookCopie, Profile, Book

# Serializers define the API representation.
class BookCopieSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = BookCopie
        fields = ['bookCondition','availability', 'createdAt', 'userId', 'bookId']
        depth = 1

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'first_name', 'last_name', 'date_joined']

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['user','zipCode', 'town', 'phone', 'latlng']
        depth = 1

class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ['title','author', 'category', 'description', 'isbn', 'img']