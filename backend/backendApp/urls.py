from django.urls import path, include
from backendApp import views
from rest_framework import routers
#from backendApp.views import BookList

router = routers.DefaultRouter()
router.register(r"BookCopie", views.BookCopieViewSet)
router.register(r"Profile",views.ProfileViewSet)
router.register(r"Book", views.BookViewSet)
router.register(r'User', views.UserViewSet)
#router.register(r'BookList', views.BookList)


urlpatterns = [
    path('',views.index, name="index"),
    path("v1/", include(router.urls)),
#    path('booklist/', BookList.as_view()),
]