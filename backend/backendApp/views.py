from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import viewsets
from django.contrib.auth.models import User
from backendApp.models import BookCopie, Profile, Book
from backendApp.serializers import BookCopieSerializer, BookSerializer, ProfileSerializer, UserSerializer

def index(request):
    return HttpResponse("bienvenue !")

# ViewSets define the view behavior.
class BookCopieViewSet(viewsets.ModelViewSet):
    queryset = BookCopie.objects.all()
    serializer_class = BookCopieSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    #permission_classes = [permissions.DjangoModelPermissions]

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

from django.views.generic import ListView

#class BookList(ListView):
#    model = Book